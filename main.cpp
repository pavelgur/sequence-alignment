#include <iostream>

#include <ctime>
#include "lib.h"

static const QMap<QChar, int> Char2Int = {{'A', 0}, {'G', 1}, {'C', 2}, {'T', 3}};

static QVector<int> Transform(const QString& s) {
    QVector<int> res;
    for (const QChar ch : s) {
        auto it = Char2Int.find(ch);
        if (it == Char2Int.end())
            throw myexc() << "Wrong symbol: " << ch;
        res.push_back(*it);
    }
    return res;
}

static const QVector< QVector<qreal> > similarity = {
    //  A   G   C   T
      {10, -1, -3, -4}, // A
      {-1,  7, -5, -3}, // G
      {-3, -5,  9, -5}, // C
      {-4, -3, -5,  8}, // T
};

static const QString testString1 = "ATCGATCATGCTAGCACTACGTAGCGACGATGCAGT";
static const QString testString2 = "AGATGGCGCAATGATGCTAGCTAG";

int main(int argc, char* argv[]) {
    const QVector<int> a = Transform(argc >= 3 ? QString(argv[1]).toUpper() : testString1);
    const QVector<int> b = Transform(argc >= 3 ? QString(argv[2]).toUpper() : testString2);
    if (argc < 3) {
        std::cout << "Sequences not specified (expected format <seq1> <seq2> <penaltyRho = -5> <penaltySigma = -5> <global/local>); perfoming test run:\n";
        std::cout << testString1.toLocal8Bit().data() << " " << testString2.toLocal8Bit().data() << ", penaltyRho=-1, penaltySigma=-1\n";
    }
    TNWAlgo algo(a, b, similarity, argc >= 4 ? QString(argv[3]).toDouble() : -5,
        argc >= 5 ? QString(argv[4]).toDouble() : -5, argc >= 6 ? (QString(argv[5]) == "global") : false);
    std::cout << algo.CalcResult().toLocal8Bit().data() << std::endl;
    return 0;
}
