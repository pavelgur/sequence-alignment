#-------------------------------------------------
#
# Project created by QtCreator 2014-10-21T14:09:13
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = SequenceAlignment
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += c++11

TEMPLATE = app


SOURCES += main.cpp \
    lib.cpp

HEADERS += \
    lib.h
