#include "lib.h"

TNWAlgo::TNWAlgo(const QVector<int>& a, const QVector<int>& b, const TMatrix& similarity, const qreal penaltyRho, const qreal penaltySigma, const bool doGlobal)
    : Aseq(a)
    , Bseq(b)
    , PenaltyRho(penaltyRho)
    , PenaltySigma(penaltySigma)
    , Global(doGlobal)
    , Similarity(similarity)
    , Matrix(a.size(), QVector<qreal>(b.size(), 0))
    , AfinnePenalties(a.size(), QVector<qreal>(b.size(), 0))
    , IsCalculated(a.size(), QVector<bool>(b.size(), false))
    , Ways(a.size(), QVector<EFromWhere>(b.size(), FW_DIAG))
    , DNAsymbols("AGCT")
{
    for (int i = 0; i < Matrix.size(); ++i) {
        Matrix[i][0] = AfinnePenalties[i][0] = PenaltyRho + PenaltySigma * i;
        Ways[i][0] = FW_UP;
    }
    for (int i = 0; i < Matrix.front().size(); ++i) {
        Matrix[0][i] = AfinnePenalties[0][i] = PenaltyRho + PenaltySigma * i;
        Ways[0][i] = FW_LEFT;
    }
    Matrix[0][0] = 0;
}

void TNWAlgo::Calc(const int i, const int j) {
    if (IsCalculated[i][j])
        return;
    if (i > 0 && j > 0)
        Calc(i - 1, j - 1);
    if (i > 0)
        Calc(i - 1, j);
    if (j > 0)
        Calc(i, j - 1);
    const qreal delta[3] = {
        Similarity[Aseq[i]][Bseq[j]],
        (j == 0 ? 0 : AfinnePenalties[i][j - 1]) + PenaltySigma,
        (i == 0 ? 0 : AfinnePenalties[i - 1][j]) + PenaltySigma,
    };
    const qreal toOptimize[4] = {
        (i > 0 && j > 0 ? Matrix[i - 1][j - 1] : 0) + delta[0],
        (j == 0 ? 0 : Matrix[i][j - 1]) + delta[1] + PenaltyRho,
        (i == 0 ? 0 : Matrix[i - 1][j]) + delta[2] + PenaltyRho,
        0,
    };
    int bestIndex = 0;
    for (int ind = 1; ind <= (Global ? 2 : 3); ++ind) {
        if (toOptimize[ind] >= toOptimize[bestIndex])
            bestIndex = ind;
    }
    switch ((EFromWhere)bestIndex) {
    case FW_LEFT: case FW_UP:
        AfinnePenalties[i][j] = delta[bestIndex];
        break;
    default:
        AfinnePenalties[i][j] = 0;
    }
    Ways[i][j] = (EFromWhere)bestIndex;
    IsCalculated[i][j] = true;
    Matrix[i][j] = toOptimize[bestIndex];
}

QString TNWAlgo::CalcResult() {
    int i = Matrix.size() - 1;
    int j = Matrix.front().size() - 1;
    Calc(i, j);
    QString alignedA;
    QString alignedB;
    if (!Global) {
        const QPair<int,int> bestIds = [&] () {
            QPair<int,int> res(0, 0);
            for (int i = 0; i < Matrix.size(); ++i) {
                for (int j = 0; j < Matrix.front().size(); ++j) {
                    if (Matrix[i][j] >= Matrix[res.first][res.second]) {
                        res.first = i;
                        res.second = j;
                    }
                }
            }
            return res;
        } ();
        alignedA.push_back('|');
        alignedB.push_back('|');
        i = bestIds.first + 1;
        j = bestIds.second + 1;
        while (i < Matrix.size() || j < Matrix.front().size()) {
            if (i < Matrix.size())
                alignedA.push_back(DNAsymbols[Aseq[i++]]);
            else
                alignedA.push_back('_');
            if (j < Matrix.front().size())
                alignedB.push_back(DNAsymbols[Bseq[j++]]);
            else
                alignedB.push_back('_');
        }
        i = bestIds.first;
        j = bestIds.second;
    }
    qreal score = 0;
    while (i >= 0 && j >= 0) {
        const EFromWhere fromWhere = Ways[i][j];
        if (fromWhere == FW_LOCAL_BOUND)
            break;
        if (fromWhere != FW_LEFT)
            alignedA.push_front(DNAsymbols[Aseq[i]]);
        else
            alignedA.push_front('_');
        if (fromWhere != FW_UP)
            alignedB.push_front(DNAsymbols[Bseq[j]]);
        else
            alignedB.push_front('_');
        if (fromWhere == FW_DIAG || fromWhere == FW_LOCAL_BOUND)
            score += Similarity[Aseq[i]][Bseq[j]];
        else
            score += AfinnePenalties[i][j];
        if (fromWhere != FW_LEFT)
            --i;
        if (fromWhere != FW_UP)
            --j;
    }
    if (Global) {
        const int lengthDiff = qMax(i + 1, 0) + qMax(j + 1, 0);
        score += (2 * AfinnePenalties[qMax(i, 0)][qMax(j, 0)] + PenaltySigma * (lengthDiff - 1)) * lengthDiff / 2 ;
    } else {
        alignedA.push_front('|');
        alignedB.push_front('|');
    }
    while (i >= 0 || j >= 0) {
        if (i >= 0)
            alignedA.push_front(DNAsymbols[Aseq[i--]]);
        else
            alignedA.push_front('_');
        if (j >= 0)
            alignedB.push_front(DNAsymbols[Bseq[j--]]);
        else
            alignedB.push_front('_');
    }
    return QString("Similarity score: ") + QString::number(score) + "\n" + alignedA + "\n" + alignedB;
}
