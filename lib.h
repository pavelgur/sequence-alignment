#pragma once

#include <exception>

#include <QRunnable>
#include <QVector>
#include <QString>
#include <QThreadPool>
#include <QMap>
#include <QQueue>
#include <QDebug>

class myexc : public std::exception {
public:
    myexc(const QString& what = "")
        :WAT(what)
    {}

    ~myexc() noexcept {}
    const char* what() const noexcept override {
        return WAT.toLocal8Bit().data();
    }
    myexc& operator<< (const QString& s) {
        WAT.append(s);
        return *this;
    }

private:
    QString WAT;
};

class TNWAlgo {
    enum EFromWhere {
        FW_DIAG = 0,
        FW_LEFT = 1,
        FW_UP   = 2,
        FW_LOCAL_BOUND = 3,
    };

    typedef QVector< QVector<qreal> > TMatrix;
    typedef QVector< QVector<bool> >  TBitMatrix;
    typedef QVector< QVector<EFromWhere> > TWayMatrix;

public:
    TNWAlgo(const QVector<int>& a, const QVector<int>& b, const TMatrix& similarity, const qreal penaltyRho, const qreal penaltySigma, const bool doGlobal = true);

    QString CalcResult();

private:
    void Calc(const int i, const int j);
    QPair<int, int> GetBiggerIndcs() const;

private:
    const QVector<int> Aseq;
    const QVector<int> Bseq;
    const qreal PenaltyRho;
    const qreal PenaltySigma;
    const bool Global;
    const TMatrix& Similarity;
    TMatrix Matrix;
    TMatrix AfinnePenalties;
    TBitMatrix IsCalculated;
    TWayMatrix Ways;
    const QString DNAsymbols;
};
